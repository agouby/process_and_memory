#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <limits.h>
#include <inttypes.h>
#include <string.h>

#define PATH_SIZE 50
#define NCHILDREN 20

struct pid_path {
	char *path;
	size_t bufsize;
};

struct pid_info {
	int pid;
	int parent_pid;
	int state;
	void const *stack;
	uint64_t age;
	int *children;
	struct pid_path root;
	struct pid_path pwd;
};

int	get_pid_info(struct pid_info *ret, int pid)
{
	return (int)syscall(335, ret, pid);
}

ssize_t	get_pid_children(int pid, int *children, size_t count)
{
	return (ssize_t)syscall(336, pid, children, count);
}

char	*fetch_state(int state)
{
	switch (state) {
		case -1:
			return "unrunnable";
		case 0:
			return "runnable";
	}
	return "stopped";
}

int	init_paths(struct pid_path *root, struct pid_path *pwd)
{
	root->path = malloc(PATH_SIZE);
	if (!root->path)
		return -1;
	root->bufsize = PATH_SIZE;
	pwd->path = malloc(PATH_SIZE);
	if (!pwd->path) {
		free(root->path);
		return -1;
	}
	pwd->bufsize = PATH_SIZE;
	return 0;
}

void	print_pid_info(int pid)
{
	struct pid_info p_info;
	int ret;
	ssize_t nchild;

	memset(&p_info, 0, sizeof(p_info));

	if (init_paths(&p_info.root, &p_info.pwd))
		goto err;

	p_info.children = malloc(NCHILDREN);
	if (!p_info.children)
		goto err;

	printf("\n\n*** PID_INFOS ***\n\n");
	ret = get_pid_info(&p_info, pid);
	if (ret)
		goto err;
	nchild = get_pid_children(pid, p_info.children, NCHILDREN);
	if (nchild == -1) {
		ret = (int)nchild;
		goto err;
	}
	printf("PID = %d\n", p_info.pid);
	printf("PID PARENT = %d\n", p_info.parent_pid);
	printf("STACK = %p\n", p_info.stack);
	printf("STATE = %d %s\n", p_info.state, fetch_state(p_info.state));
	printf("AGE = %lu\n", p_info.age);
	printf("ROOT_PATH = %s\n", p_info.root.path);
	printf("PWD_PATH = %s\n", p_info.pwd.path);
	if (nchild) {
		printf("CHILDREN:\n");
		for (ssize_t i = 0; i < nchild; i++) {
			printf("\t%d\n", p_info.children[i]);
		}
		printf("\n");
		for (ssize_t i = 0; i < nchild; i++) {
			print_pid_info(p_info.children[i]);
		}
	}
	goto end;

err:
	printf("Error %d\n", ret);
end:	
	free(p_info.root.path);
	free(p_info.pwd.path);
	free(p_info.children);
}

int	main(int ac, char **av)
{
	if (ac < 2)
	{
		printf("Give PID plz\n");
		return 1;
	}
	print_pid_info(atoi(av[1]));
	return 0;
}

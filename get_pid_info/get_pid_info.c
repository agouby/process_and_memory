#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/fs_struct.h>
#include <linux/slab.h>
#include <linux/limits.h>
#include "get_pid_info.h"

static long _get_path(struct dentry *dent, char *path_buf,
		     const struct pid_path *ps_src, struct pid_path *ps_dest)
{
	char *path_ptr;
	size_t pathlen;
	
	path_ptr = dentry_path_raw(dent, path_buf, PATH_MAX);
	if (IS_ERR(path_ptr))
		return (long)path_ptr;
	pathlen = strlen(path_ptr);
	if (ps_src->bufsize <= pathlen)
		return -ENOMEM;
	/* pathlen + 1 to copy the \0.*/
	if (copy_to_user(ps_dest->path, path_ptr, pathlen + 1))
		return -EFAULT;
	return 0;
}

/*
 * int get_pid_info(struct pid_info __user *retinfo, int pid)
 */

SYSCALL_DEFINE2(get_pid_info, struct pid_info __user*, retinfo, int, pid)
{
	int ret;
	char *path_buf;
	struct pid *pid_struct;
	struct pid_info info_tmp;
	struct task_struct *task;
	struct path pwd, root;
	int state;
	int pid_tmp;

	pid_struct = find_get_pid(pid);
	if (!pid_struct)
		return -ESRCH;

	task = get_pid_task(pid_struct, PIDTYPE_PID);
	if (!task)
		return -ESRCH;
	if (!task->fs)
		return -ENOENT;

	if (copy_from_user(&info_tmp, retinfo, sizeof(struct pid_info)))
		return -EFAULT;

	get_fs_root(task->fs, &root);
	get_fs_pwd(task->fs, &pwd);

	path_buf = kmalloc(PATH_MAX, GFP_KERNEL);
	if (!path_buf)
		return -ENOMEM;
	ret = _get_path(root.dentry, path_buf, &info_tmp.root, &retinfo->root);
	if (ret)
		goto getpath_failed;
	ret = _get_path(pwd.dentry, path_buf, &info_tmp.pwd, &retinfo->pwd);
	if (ret)
		goto getpath_failed;
	kfree(path_buf);

	pid_tmp = task_pid_vnr(task);
	if (copy_to_user(&retinfo->pid, &pid_tmp, sizeof(pid_tmp)))
		return -EFAULT;

	pid_tmp = task_pid_vnr(task->real_parent);
	if (copy_to_user(&retinfo->parent_pid, &pid_tmp, sizeof(pid_tmp)))
		return -EFAULT;

	state = (task->state > 0) ? 1: task->state;
	if (copy_to_user(&retinfo->state, &state, sizeof(state)))
		return -EFAULT;

	if (copy_to_user(&retinfo->age, &task->start_time, sizeof(uint64_t)))
		return -EFAULT;
	
	if (copy_to_user(&retinfo->stack, &task->stack, sizeof(task->stack)))
		return -EFAULT;

	return 0;

getpath_failed:
	kfree(path_buf);
	return ret;
}

/*
 * ssize_t get_pid_children(int pid, int *children, size_t count)
 */

SYSCALL_DEFINE3(get_pid_children, int, pid, int __user*, children, size_t, cnt)
{
	struct pid *pid_struct;
	struct task_struct *task;
	struct task_struct *child = NULL;
	size_t i = 0;

	pid_struct = find_get_pid(pid);
	if (!pid_struct)
		return -ESRCH;

	task = get_pid_task(pid_struct, PIDTYPE_PID);
	if (!task)
		return -ESRCH;

	task_lock(task);
	list_for_each_entry(child, &task->children, sibling) {
		if (i == cnt)
			break ;
		if (copy_to_user(&children[i], &child->pid, sizeof(pid))) {
			task_unlock(task);
			return -EFAULT;
		}
		++i;
	}
	task_unlock(task);
	return i;
}

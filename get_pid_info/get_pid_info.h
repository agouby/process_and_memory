#ifndef GET_PID_INFO_H
#define GET_PID_INFO_H

struct pid_path {
	char *path;
	size_t bufsize;
};

struct pid_info {
	int pid;
	int parent_pid;
	int state;
	void const *stack;
	uint64_t age;
	int *children;
	struct pid_path root;
	struct pid_path pwd;
};

#endif
